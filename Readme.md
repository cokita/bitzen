#Sejam Bem Vindos

#### Esse projeto foi desenvolvido em virtude do processo de seleção para a vaga de Programador(a) pleno (remoto) para a empresa Bitzen.

### Nome: Ana Flávia Araújo Lima Pinto de Carvalho
#### Telefone: 55 61 996180178

### Resumo dos requisitos

> Deverá ser desenvolvido uma API que possibilite o cadastro de veículos para que posteriormente seja feito o lançamento de abastecimentos deles, com o objetivo final de gerar um relatório para acompanhar as informações de consulto e gastos de cada veículo individualmente.
A API a ser desenvolvida deverá utilizar as tecnologias informadas no documento. Nem todas são obrigatórias, porém tudo o que for utilizado contará no momento da avaliação.
O projeto pronto deverá ser disponibilizado em um repositório privado Git através do GitHub e compartilhado conosco para que possamos avaliar.
>

### Tecnologias utilizadas

1. PHP 7.3 (na imagem do docker, mas funciona com PHP > 7.2.5)
2. Laravel 7.x (RESTFUL)
3. Mysql
4. Postman Collections
5. GIT
6. Docker

### Requisitos para rodar em ambiente local

1. PHP >= 7.2.5
2. BCMath PHP Extension
3. Ctype PHP Extension
4. Fileinfo PHP extension
5. JSON PHP Extension
6. Mbstring PHP Extension
7. OpenSSL PHP Extension
8. PDO PHP Extension
9. Tokenizer PHP Extension
10. XML PHP Extension
11. Mysql PDO
12. Apache ou Nginx (já configurado)
13. Vhost apontando para a pasta public do projeto (ex.: ./bitzen/www/public)
14. GIT

### Requisitos para rodar em container (Docker)

1. GIT
2. DOCKER

### Para rodar (em container):

1. Faça o clone do projeto 
>git clone https://cokita@bitbucket.org/cokita/bitzen.git
2. Entre na pasta
> cd bitzen
3. Execute o comando:
>docker-compose up -d 
>Aguarde, esse processo demora vários minutos.
4. Entre no container:
>docker exec -it bitzen bash
5. Dentro do /var/www/html (já deve estar nele):
>composer install
6. Copie o .env.example para .env
>cp .env.example .env
7. Entre no arquivo .env (recem criado), e informe a senha do banco no campo: "DB_PASSWORD" 
8. Execute o comando:
>php artisan key:generate
9. Dê permissões nas pastas storage e bootstrap/cache
>chmod -R 777 storage bootstrap/cache

## A partir daqui, o projeto já está rodando na url: http://localhost:8000
10. O banco está populado, caso deseje recriar as tabelas e popular novamente para testar o migration/seeds, execute o comando
a seguir:

> php artisan migrate:refresh --seed

Ou se preferir, drop as tabelas e execute os comando individualmente:

>php artisan migrate

    PS: Caso o banco esteja populado e deseje testar o migration + seeds, delete todas as tabelas do banco e
    execute o comando acima, seguido de:

>php artisan db:seed

    PS: Criei um seed para popular as tabelas de veículos e de abastecimentos, para melhor visualização dos relatório.
    Caso deseje esses seeds, basta descomentar as linhas:
    
            // $this->call(VehiclesSeeder::class);
            // $this->call(SuppliesSeeder::class);
    
    Do arquivo: ./www/database/seeds/DatabaseSeeder.php
    
Para dropar todas as tabelas manualmente:
```
drop table supplies;
drop table vehicles;
drop table models;
drop table brand_type_vehicle;
drop table brands;
drop table files;
drop table type_vehicle;
drop table oauth_access_tokens;
drop table oauth_auth_codes;
drop table oauth_clients;
drop table oauth_personal_access_clients;
drop table oauth_refresh_tokens;
drop table users;
drop table failed_jobs;
drop table migrations;
```
11. Para popular os clients do passport, responsável pela autenticação OAUTH.
>php artisan passport:install
12. Caso o comando acima seja executado, deve-se pegar a chave 2 gerada (elas estão armazenadas no banco, tabela oauth_clients)
e adicionar no body do endpoint do login: oauth/token como mostrado no json de exemplo:

    ```
    {
    	"grant_type":"password",
    	"client_id":2,
    	"client_secret":"aeYZTWnudjojAVENMfjDmFVwnj2bOqEuSUzZKdWf",
    	"username":"cokitabr@gmail.com",
    	"password":"abcd1234"
    }
    ```

13. Pronto, o projeto está pronto para uso.

### Executando os testes unitários
**Criei alguns testes, mas quero deixar claro que testes unitários não são meu forte, justamente por não ter experiência.
Aqui em Brasília não se tem essa cultura, então tudo que aprendi foi estudando.**

1. Execute os comandos abaixo:
```
docker exec -it bitzen bash (caso não esteja no container)
php artisan config:cache
php artisan test
```
PS: todas as vezes que o teste é executado, ele invoca os migrations e os seeds e isso leva um tempo muito grande.
Caso deseje, execute o teste uma vez e depois vá no arquivo: ./www/tests/TestCase.php e mude a variável "$setUpRun" para true.
Ele não deletará o banco e os testes ficarão bem mais rápidos.

### Utilizando os endpoints: [Lista dos Endpoints](./www)

### MER

![](./www/MER_bitzen.jpeg)

## Postman Collections

* A collection com os enpoints do Postman, está na [pasta ./PostmanCollections](./PostmanCollections)
* Lá também pode-se encontrar uma variável global para o token, sempre utilizo, pois uma vez logado, basta selecionar o token, clicar com o botão direito do mouse e adicionar na variável.
* Tem também uma variavel de ambiente chamada {{url}}, que deve ter a URL do servidor local, no meu caso: http://localhost:8000, caso não altere no docker, será a mesma.
