<?php

namespace App\Constants;

class ConstTypeVehicle {

    CONST CAR = 1;
    CONST MOTOCYCLE = 2;
    CONST TRUCK = 3;
    CONST NAUTICAL = 4;

}
