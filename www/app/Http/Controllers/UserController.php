<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $data = collect(request()->all());
            $page = $data->get('page') ?? 1;
            if ($data->get('with')) {
                $with = explode(',', $data['with']);
                $user = User::with($with);
            } else {
                $user = User::query();
            }

            if ($data->get('id')) {
                $user->where('id', '=', $data->get('id'));
            }

            if ($data->get('name')) {
                $user->where('name', 'LIKE', "%{$data->get('name')}%");
            }

            if ($data->get('email')) {
                $user->where('email', '=', $data->get('email'));
            }

            $user->where('active', '=', 1);
            return response()->json([
                'status' => 'success',
                'data' => $user->paginate(20, ['*'], 'page', $page)
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = User::find($id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            validate($request->all(), [
                'email' => 'unique:users'
            ]);
            $user = User::find($id);
            if (!$user)
                throw new \Exception("Usuário não encontrado!", 412);

            $user->fill($request->all());
            $user->save();

            return response()->json([
                'status' => 'success',
                'data' => $user
            ]);

        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);
            if (!$user)
                throw new \Exception("Usuário não encontrado!", 412);

            $user->active = 0;
            $user->save();

            return response()->json([
                'status' => 'success',
                'data' => $user
            ]);

        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Object
     * {
     *      "email": "andrevini@gmail.com",
     *      "name" : "André Vinicius da Silva Caixeta",
     *      "password": "abcd1234"
     *   }
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            validate($request->all(), [
                'email' => 'required|unique:users',
                'name' => 'required',
                'password' => 'required'
            ]);

            $user = new User;
            $user->email = $request->email;
            $user->name = $request->name;
            $user->password = bcrypt($request->password);
            $user->save();

            DB::commit();
            return response()->json([
                'status' => 'success',
                'data' => $user
            ]);

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 400);
        }
    }
}
