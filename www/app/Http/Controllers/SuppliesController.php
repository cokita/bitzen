<?php

namespace App\Http\Controllers;

use App\Models\Supplies;
use App\Models\Vehicle;
use App\Services\SuppliesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SuppliesController extends Controller
{
    /**
     * Display a listing of the Supplies.
     * Esse método só deve ser usado por usuários administradores, uma vez que ele retorna vários abastecimentos
     * e não somente do do usuario logado.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $data = collect(request()->all());
            $suppliesService = new SuppliesService();
            $result = $suppliesService->searchSupplies($data);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = collect(request()->all());
        if ($data->get('with')) {
            $with = explode(',', $data['with']);
            $vehicle = Supplies::with($with)->find($id);
        } else {
            $vehicle = Supplies::find($id);
        }
        return response()->json([
            'status' => 'success',
            'data' => $vehicle
        ], 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $supply = Supplies::find($id);
            if (!$supply)
                throw new \Exception("Abastecimento não encontrado!", 412);

            if($supply->user_id != auth()->user()->id)
                throw new \Exception("O abastecimento só pode ser atualizados pelo usuário logado.");

            $vehicle_id = $supply->vehicle_id;
            $supply->fill($request->all());
            $supply->vehicle_id = $vehicle_id;
            $supply->save();

            return response()->json([
                'status' => 'success',
                'data' => $supply
            ]);

        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $supply = Supplies::find($id);
            if (!$supply)
                throw new \Exception("Abastecimento não encontrado!", 412);

            $supply->active = 0;
            $supply->save();

            return response()->json([
                'status' => 'success',
                'data' => $supply
            ]);

        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = collect(request()->all());
            validate($data->toArray(), [
                'vehicle_id' => 'required',
                'type_fuel' => 'required',
                'amount' => 'required',
                'current_mileage' => 'required',
                'liters' => 'required',
                'fuel_station' => 'required',
            ], [
                'vehicle_id.required' => 'O veículo é obrigatório',
                'type_fuel.required' => 'O tipo de combustível do veículo é obrigatório',
                'amount.required' => 'O valor total do abastecimento é obrigatório',
                'current_mileage.required' => 'A quilometragem atual do veículo é obrigatório',
                'liters.required' => 'A quantdade de litros abastecidos é obrigatório',
                'fuel_station.required' => 'O posto de combustível é obrigatório',
            ]);
            $vehicle = Vehicle::find($data->get('vehicle_id'));
            if(!$vehicle)
                throw new \Exception("Veículo não encontrado.");

            if($vehicle->mileage > $data->get('current_mileage'))
                throw new \Exception("A quilometragem atual não pode ser menor do que a cadastrada inicialmente.");

            if($vehicle->user_id != auth()->user()->id)
                throw new \Exception("O abastecimento só pode ser realizado para veículos que pertencem ao usuário logado.");

            $lastSupplie = Supplies::query()->where('vehicle_id', $data->get('vehicle_id'))->orderByDesc('created_at')->first();
            if($lastSupplie){
                if($lastSupplie->current_mileage > $data->get('current_mileage'))
                    throw new \Exception("A quilometragem atual não pode ser menor do que a do último abastecimento: ".$lastSupplie->current_mileage. " quilometros.");
            }

            $supplies = new Supplies();
            $supplies->fill($request->all());
            $supplies->user_id = auth()->user()->id;

            $supplies->save();

            DB::commit();
            return response()->json([
                'status' => 'success',
                'data' => $supplies
            ]);

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 400);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOnlyMySupplies() {
        try {
            $data = collect(request()->all());
            $suppliesService = new SuppliesService();
            $result = $suppliesService->searchSupplies($data, true);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
