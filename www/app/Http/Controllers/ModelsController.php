<?php

namespace App\Http\Controllers;
use App\Models\Models;
use App\Models\User;


class ModelsController extends Controller
{
    /**
     * Display a listing of the Cars Models.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $data = collect(request()->all());
            if ($data->get('with')) {
                $with = explode(',', $data['with']);
                $model = Models::with($with);
            } else {
                $model = Models::query();
            }

            if ($data->get('id')) {
                $model->where('id', '=', $data->get('id'));
            }

            if ($data->get('name')) {
                $model->where('name', 'LIKE', "%{$data->get('name')}%");
            }

            if ($data->get('brand_id')) {
                $model->where('brand_id', $data->get('brand_id'));
            }

            $model->where('active', '=', 1);
            $model->orderBy('name');
            if($data->get('paginate')) {
                $page = $data->get('page') ?? 1;
                $result = $model->paginate(20, ['*'], 'page', $page);
            }else {
                $result = $model->get();
            }
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified Model.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $model = Models::find($id);
        return response()->json([
            'status' => 'success',
            'data' => $model
        ], 400);
    }
}
