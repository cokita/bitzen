<?php

namespace App\Http\Controllers;
use App\Models\Brands;
use App\Models\BrandTypeVehicle;
use App\Models\TypeVehicle;


class BrandsController extends Controller
{
    /**
     * Display a listing of the Brands.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $data = collect(request()->all());
            if ($data->get('with')) {
                $with = explode(',', $data['with']);
                $brand = Brands::with($with);
            } else {
                $brand = Brands::query();
            }

            if ($data->get('id')) {
                $brand->where('id', '=', $data->get('id'));
            }

            if ($data->get('name')) {
                $brand->where('name', 'LIKE', "%{$data->get('name')}%");
            }

            $brand->where('active', '=', 1);

            if($data->get('paginate')) {
                $page = $data->get('page') ?? 1;
                $result = $brand->paginate(20, ['*'], 'page', $page);
            }else {
                $result = $brand->get();
            }

            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified brand.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $brands = Brands::find($id);
        return response()->json([
            'status' => 'success',
            'data' => $brands
        ]);
    }

    /**
     * Display a listing of the Brands filtered by vehicle type.
     *
     * @param $idTypeVehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function brandsByTypeVehicle($idTypeVehicle) {
        try{
            $typeVehicle = TypeVehicle::find($idTypeVehicle);

            if(!$typeVehicle)
                throw new \Exception("Tipo de veículo não encontrado!");

            $brandsTypeVehicle = BrandTypeVehicle::with(['brand'])
                ->where('type_vehicle_id', $typeVehicle->id)
                ->get();

            return response()->json([
                'status' => 'success',
                'data' => $brandsTypeVehicle
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
