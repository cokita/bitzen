<?php

namespace App\Http\Controllers;

use App\Services\ReportService;

class ReportsController extends Controller
{
    /**
     * Display a report of liters by month, you can specify what year do you want to see.
     * Per logged user
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportLitersByMonth() {
        try {
            $data = collect(request()->all());
            $reportService = new ReportService();
            $result = $reportService->reportLitersByMonth($data);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display a report of amount spend by month, you can specify what year do you want to see.
     * Per logged user
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportAmountByMonth() {
        try {
            $data = collect(request()->all());
            $reportService = new ReportService();
            $result = $reportService->reportAmountByMonth($data);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display a report of kilometers spend by month, you can specify what year do you want to see.
     * Per logged user and per vehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportKilometersByMonthByCar() {
        try {
            $data = collect(request()->all());
            $reportService = new ReportService();
            $result = $reportService->reportKilometersByMonthByCar($data);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }


    /**
     * Display a report of kilometers spend by month, you can specify what year do you want to see.
     * Per logged user and all cars
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportKilometersByMonthAllCars() {
        try {
            $data = collect(request()->all());
            $reportService = new ReportService();
            $result = $reportService->reportKilometersByMonthAllCars($data);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Required:
     * Display a report of kilometers per Liters a vehicle spend by month, you can specify what year do you want to see.
     * Per logged user and all cars
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportAverageSpent($vehicle_id) {
        try {
            $data = collect(request()->all());
            $reportService = new ReportService();
            $result = $reportService->reportAverageSpent($data, $vehicle_id);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
