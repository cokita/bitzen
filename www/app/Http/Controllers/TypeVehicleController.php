<?php

namespace App\Http\Controllers;

use App\Models\TypeVehicle;

class TypeVehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $data = collect(request()->all());
            $typeVehicle = TypeVehicle::query();

            if ($data->get('id')) {
                $typeVehicle->where('id', '=', $data->get('id'));
            }

            if ($data->get('name')) {
                $typeVehicle->where('name', 'LIKE', "%{$data->get('name')}%");
            }

            $typeVehicle->where('active', '=', 1);
            return response()->json([
                'status' => 'success',
                'data' => $typeVehicle->get()
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $typeVehicle = TypeVehicle::find($id);
        return response()->json([
            'status' => 'success',
            'data' => $typeVehicle
        ], 400);
    }
}
