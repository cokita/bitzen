<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Services\FileService;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{

    public function store(){
        DB::beginTransaction();
        try {
            $data = collect(request()->all());

            validate($data->toArray(), [
                'file' => 'required',
                'path' => 'required',
            ],[
                'file' => 'Favor informar o arquivo.',
                'path' => 'Favor informar o path do arquivo.'
            ]);

            $fileService = new FileService();
            $file = $fileService->salvar($data->get('file'), $data->get('path'), $data->get('file_type_id'));

            $fileService->uploadImageByBinary($data->get('file'), $file);

            DB::commit();
            return response()->json([
                'status' => 'success',
                'data' => $file
            ]);

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $file = File::find($id);
        return response()->json([
            'status' => 'success',
            'data' => $file
        ], 400);
    }
}
