<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Services\VehicleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class VehicleController extends Controller
{
    /**
     * Display a listing of the Vehicles.
     * Esse método só deve ser usado por usuários administradores, uma vez que ele retorna vários veiculos
     * e não somente do do usuario logado.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $data = collect(request()->all());
            $vehicleService = new VehicleService();
            $result = $vehicleService->searchVehicle($data);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = collect(request()->all());
        if ($data->get('with')) {
            $with = explode(',', $data['with']);
            $vehicle = Vehicle::with($with)->find($id);
        } else {
            $vehicle = Vehicle::find($id);
        }
        return response()->json([
            'status' => 'success',
            'data' => $vehicle
        ], 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $vehicle = Vehicle::find($id);
            if (!$vehicle)
                throw new \Exception("Veículo não encontrado!", 412);

            $vehicle->fill($request->all());
            $vehicle->save();

            return response()->json([
                'status' => 'success',
                'data' => $vehicle
            ]);

        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $vehicle = Vehicle::find($id);
            if (!$vehicle)
                throw new \Exception("Veículo não encontrado!", 412);

            $vehicle->active = 0;
            $vehicle->save();

            return response()->json([
                'status' => 'success',
                'data' => $vehicle
            ]);

        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Object
     * {
     *      "model_id": "andrevini@gmail.com",
     *      "type_vehicle_id" : 1,
     *      "year": 2020,
     *      "mileage": 65000,
     *      "license_plate": "JPQ1112",
     *      "type_fuel": "F"
     *   }
     * Tipos de Combustivel: F -> Flex, G -> Gasolina, A -> Alcool, D -> Diesel, E -> Eletrico
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            validate($request->all(), [
                'model_id' => 'required',
                'type_vehicle_id' => 'required',
                'year' => 'required',
                'mileage' => 'required',
                'license_plate' => 'required',
                'type_fuel' => 'required',
            ], [
                'model_id.required' => 'O modelo do veículo é obrigatório',
                'type_vehicle_id.required' => 'O tipoe do veículo é obrigatório',
                'year.required' => 'O ano do veículo é obrigatório',
                'mileage.required' => 'A quilometragem do veículo é obrigatório',
                'license_plate.required' => 'A placa do veículo é obrigatório',
                'type_fuel.required' => 'O tipo de combustível do veículo é obrigatório',
            ]);
            $vehicle = new Vehicle();
            $vehicle->fill($request->all());
            $vehicle->user_id = auth()->user()->id;

            $vehicle->save();

            DB::commit();
            return response()->json([
                'status' => 'success',
                'data' => $vehicle
            ]);

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 400);
        }
    }

    /**
     * List of Vehicles by Logged User
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOnlyMyVehicles() {
        try {
            $data = collect(request()->all());
            $vehicleService = new VehicleService();
            $result = $vehicleService->searchVehicle($data, true);
            return response()->json([
                'status' => 'success',
                'data' => $result
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
