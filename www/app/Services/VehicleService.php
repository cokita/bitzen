<?php

namespace App\Services;

use App\Models\Vehicle;
use Illuminate\Support\Collection;

class VehicleService extends Service {

    /**
     * @param Collection $data
     * @param bool $onlyVehiclesLoggedUser
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function searchVehicle($data, $onlyVehiclesLoggedUser = false) {
        if ($data->get('with')) {
            $with = explode(',', $data['with']);
            $vehicle = Vehicle::with($with);
        } else {
            $vehicle = Vehicle::query();
        }

        if ($data->get('id')) {
            $vehicle->where('id', '=', $data->get('id'));
        }

        if ($data->get('model_id')) {
            $vehicle->where('model_id', '=', $data->get('model_id'));
        }

        if($onlyVehiclesLoggedUser){
            $vehicle->where('user_id', '=', auth()->user()->id);
        } else {
            if ($data->get('user_id')) {
                $vehicle->where('user_id', '=', $data->get('user_id'));
            }
        }

        if ($data->get('vehicle_type_id')) {
            $vehicle->where('vehicle_type_id', '=', $data->get('vehicle_type_id'));
        }

        if ($data->get('year')) {
            $vehicle->where('year', '=', $data->get('year'));
        }

        if ($data->get('license_plate')) {
            $vehicle->where('license_plate', '=', $data->get('license_plate'));
        }

        if ($data->get('type_fuel')) {
            $vehicle->where('type_fuel', '=', $data->get('type_fuel'));
        }

        $vehicle->where('active', 1);
        if($data->get('paginate')) {
            $page = $data->get('page') ?? 1;
            $result = $vehicle->paginate(20, ['*'], 'page', $page);
        }else {
            $result = $vehicle->get();
        }

        return $result;
    }

}
