<?php

namespace App\Services;

use App\Models\Supplies;
use Illuminate\Support\Collection;

class ReportService extends Service {

    public function reportLitersByMonth(Collection $data)
    {
        $sql = Supplies::query()
            ->selectRaw('YEAR(created_at) as year')
            ->selectRaw('MONTH(created_at) as month')
            ->selectRaw('sum(liters) as total')
            ->groupByRaw('YEAR(created_at)')
            ->groupByRaw('MONTH(created_at)');

        if($data->get('year')){
            $sql->whereRaw('YEAR(created_at) = '.$data->get('year'));
        }

        $sql->where('user_id', auth()->user()->id);

        return $sql->get();

    }

    public function reportAmountByMonth(Collection $data)
    {
        $sql = Supplies::query()
            ->selectRaw('YEAR(created_at) as year')
            ->selectRaw('MONTH(created_at) as month')
            ->selectRaw('sum(amount) as total')
            ->groupByRaw('YEAR(created_at)')
            ->groupByRaw('MONTH(created_at)');

        if($data->get('year')){
            $sql->whereRaw('YEAR(created_at) = '.$data->get('year'));
        }

        $sql->where('user_id', auth()->user()->id);

        return $sql->get();

    }

    /**
     *
     * @param Collection $data
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function reportKilometersByMonthByCar(Collection $data)
    {
        $sql = Supplies::query()->with('vehicle')
            ->select(['user_id', 'vehicle_id'])
            ->selectRaw('YEAR(created_at) as year')
            ->selectRaw('MONTH(created_at) as month')
            //->selectRaw('min(current_mileage) as minimo')
            //->selectRaw('max(current_mileage) as maximo')
            ->selectRaw('(max(current_mileage) - min(current_mileage)) as totalKilometers')
            ->groupBy(['vehicle_id', 'user_id'])
            ->groupByRaw('YEAR(created_at)')
            ->groupByRaw('MONTH(created_at)');

        if($data->get('year')){
            $sql->whereRaw('YEAR(created_at) = '.$data->get('year'));
        }

        $sql->where('user_id', auth()->user()->id);

        return $sql->get();

    }

    /**
     *
     * @param Collection $data
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function reportKilometersByMonthAllCars(Collection $data)
    {
        $sql = Supplies::query()
            ->selectRaw('YEAR(created_at) as year')
            ->selectRaw('MONTH(created_at) as month')
            //->selectRaw('min(current_mileage) as minimo')
            //->selectRaw('max(current_mileage) as maximo')
            ->selectRaw('(max(current_mileage) - min(current_mileage)) as totalKilometers')
            ->groupBy(['user_id'])
            ->groupByRaw('YEAR(created_at)')
            ->groupByRaw('MONTH(created_at)');

        if($data->get('year')){
            $sql->whereRaw('YEAR(created_at) = '.$data->get('year'));
        }

        $sql->where('user_id', auth()->user()->id);

        return $sql->get();

    }

    /**
     * @param Collection $data
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function reportAverageSpent(Collection $data, $vehicle_id)
    {
        $sql = Supplies::query()
            ->selectRaw('YEAR(created_at) as year')
            ->selectRaw('MONTH(created_at) as month')
            //->selectRaw('min(current_mileage) as minimo')
            //->selectRaw('max(current_mileage) as maximo')
            ->selectRaw('(max(current_mileage) - min(current_mileage)) as totalKilometers')
            ->selectRaw('sum(liters) as totalLiters')
            ->selectRaw('(max(current_mileage) - min(current_mileage))/sum(liters) as kilomitersPerLiter')
            ->where('vehicle_id', $vehicle_id)
            ->groupBy(['vehicle_id'])
            ->groupByRaw('YEAR(created_at)')
            ->groupByRaw('MONTH(created_at)');

        if($data->get('year')){
            $sql->whereRaw('YEAR(created_at) = '.$data->get('year'));
        }

        $sql->where('user_id', auth()->user()->id);

        return $sql->get();

    }

}
