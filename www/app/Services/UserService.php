<?php

namespace App\Services;

use App\Models\User;

class UserService extends Service {

    /**
     * @var User
     */
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

}
