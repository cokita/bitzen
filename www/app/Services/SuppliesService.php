<?php

namespace App\Services;

use App\Models\Supplies;
use Illuminate\Support\Collection;

class SuppliesService extends Service {

    /**
     * @param Collection $data
     * @param bool $onlySuppliessLoggedUser
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function searchSupplies($data, $onlySuppliessLoggedUser = false) {
        if ($data->get('with')) {
            $with = explode(',', $data['with']);
            $supplies = Supplies::with($with);
        } else {
            $supplies = Supplies::query();
        }

        if ($data->get('id')) {
            $supplies->where('id', '=', $data->get('id'));
        }

        if ($data->get('vehicle_id')) {
            $supplies->where('vehicle_id', '=', $data->get('vehicle_id'));
        }

        if($onlySuppliessLoggedUser){
            $supplies->where('user_id', '=', auth()->user()->id);
        } else {
            if ($data->get('user_id')) {
                $supplies->where('user_id', '=', $data->get('user_id'));
            }
        }

        if ($data->get('type_fuel')) {
            $supplies->where('type_fuel', '=', $data->get('type_fuel'));
        }

        $supplies->where('active', 1);
        if($data->get('paginate')) {
            $page = $data->get('page') ?? 1;
            $result = $supplies->paginate(20, ['*'], 'page', $page);
        }else {
            $result = $supplies->get();
        }

        return $result;
    }

}
