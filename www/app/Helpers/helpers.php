<?php

if (!function_exists('validate')) {

    /**
     * Create a new Validator instance.
     *
     * @param  array $data
     * @param  array $rules
     * @param  array $messages
     * @param  array $customAttributes
     */
    function validate(array $data = [], array $rules = [], array $messages = [], array $customAttributes = [])
    {
        $v = validator($data, $rules, $messages, $customAttributes);

        if ($v->fails())
            abort(412, $v->messages()->first());
    }

}

if (!function_exists('cors_dd')) {
    /**
     * Wrap dd() and inject required CORS headers first.
     *
     * @param  null  $var  // Just so we have a default value.
     */
    function cors_dd($var = null)
    {

        // Send the required CORS headers.
        $origin = $_SERVER['HTTP_ORIGIN'] ?? '*';
        header("Access-Control-Allow-Origin: $origin");
        header('Access-Control-Allow-Credentials: true'); // Only needed if you are using Sanctum.

        // DD the var.
        dd($var);
    }
}
