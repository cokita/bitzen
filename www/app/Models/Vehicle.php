<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table = 'vehicles';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'user_id', 'file_id', 'model_id', 'type_vehicle_id', 'year', 'mileage', 'license_plate', 'type_fuel', 'active'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }

    public function model()
    {
        return $this->hasOne(Models::class, 'id','model_id');
    }

    public function typeVehicle()
    {
        return $this->hasOne(TypeVehicle::class, 'id','type_vehicle_id');
    }

    public function file()
    {
        return $this->hasOne(File::class, 'id','file_id');
    }
}
