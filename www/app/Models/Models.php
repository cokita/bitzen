<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    protected $table = 'models';
    protected $fillable = [
        'id', 'brand_id', 'name', 'active'
    ];

    public function brand()
    {
        return $this->hasOne(Brands::class, 'id','brand_id');
    }
}
