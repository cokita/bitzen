<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandTypeVehicle extends Model
{
    protected $table = 'brand_type_vehicle';
    protected $fillable = [
        'id', 'brand_id', 'type_vehicle_id', 'active'
    ];

    public function brand()
    {
        return $this->hasOne(Brands::class, 'id', 'brand_id');
    }

    public function typeVehicle()
    {
        return $this->hasOne(TypeVehicle::class, 'id', 'type_vehicle_id');
    }
}
