<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplies extends Model
{
    protected $table = 'supplies';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'user_id', 'vehicle_id', 'type_fuel', 'amount', 'liters', 'current_mileage', 'fuel_station','active'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'id','vehicle_id');
    }
}
