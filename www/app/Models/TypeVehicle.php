<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeVehicle extends Model
{
    protected $table = 'type_vehicle';
    protected $fillable = [
        'id', 'name', 'active'
    ];
}
