# Endpoints


Foram criados os seguintes endpoints:

* USER
```
| POST          |   api/user            |    user.store         | Cria um novo usuário
| GET|HEAD      |   api/user            |    user.index         | Lista vários usuários *1
| GET|HEAD      |   api/user/{user}     |    user.show          | Exibe os dados de um único usuário
| PUT|PATCH     |   api/user/{user}     |    user.update        | Atualiza o usuário
| DELETE        |   api/user/{user}     |    user.destroy       | "Remove" o usuário *2 
*1 lista de usuário deve ser disponibilizada apenas para perfis administradores (não implementado)
*2 não removi fisicamente o registro, apenas inativei (active = 0)
```

* Veículos
```
| POST          |   api/vehicle            |    vehicle.store         | Cria um novo veículo
| GET|HEAD      |   api/vehicle            |    vehicle.index         | Lista vérios veículos *1
| GET|HEAD      |   api/vehicle/{id}       |    vehicle.show          | Exibe os dados de um único veículo
| PUT|PATCH     |   api/vehicle/{id}       |    vehicle.update        | Atualiza o veículo
| DELETE        |   api/vehicle/{id}       |    vehicle.destroy       | "Remove" o veículo *2 
| GET|HEAD      |   api/vehicle/only-mine  |    vehicle.only-mine     | Exibe uma lista de veículos APENAS para o usuário logado 

*1 este não é o método indicado para listar veículos por usuário, uma vez que o parametro user_id não é obrigatório. Deve ser disponibilizada apenas para perfis administradores (não implementado)
*2 não removi fisicamente o registro, apenas inativei (active = 0)
```

* Abastecimentos
```
| POST          |   api/supplies            |    supplies.store         | Cria um novo abastecimento
| GET|HEAD      |   api/supplies            |    supplies.index         | Lista vários abastecimentos *1
| GET|HEAD      |   api/supplies/{id}       |    supplies.show          | Exibe os dados de um único abastecimento
| PUT|PATCH     |   api/supplies/{id}       |    supplies.update        | Atualiza o abastecimento
| DELETE        |   api/supplies/{id}       |    supplies.destroy       | "Remove" o abastecimento *2 
| GET|HEAD      |   api/supplies/only-mine  |    supplies.only-mine     | Exibe uma lista de abastecimentos APENAS para o usuário logado 

*1 este não é o método indicado para listar abastecimentos por usuário, uma vez que o parametro user_id não é obrigatório. Deve ser disponibilizada apenas para perfis administradores (não implementado)
*2 não removi fisicamente o registro, apenas inativei (active = 0)
```

* Tipos de Veículos (CARRO, MOTO, CAMINHÃO, NAÚTICO)
```
| GET|HEAD      |   api/type-vehicles            |    type-vehicles.index         | Lista vários tipos de veículos
| GET|HEAD      |   api/type-vehicles/{id}       |    type-vehicles.show          | Exibe os dados de um único tipo de veículo
```

* Marcas (Ford, Chevrolet, Fiat, etc)
```
| GET|HEAD      |   api/brands                               |    brands.index                      | Lista vários tipos de marcas
| GET|HEAD      |   api/brands/{id}                          |    brands.show                       | Exibe os dados de uma única brands
| GET|HEAD      |   api/brands/brands-by-type-vehicle/{id}   |    brands.brands-by-type-vehicle     | Exibe uma lista de marcas por tipo de veiculo
```

* Modelos (Spin, Focus, Ka, F250, etc)
```
| GET|HEAD      |   api/models            |    models.index         | Lista vários modelos de veículos
| GET|HEAD      |   api/models/{id}       |    models.show          | Exibe os dados de um único modelo
```

* Relatórios
```
| GET|HEAD      |   api/reports/liters-by-month              |    reports.liters-by-month               | Quantidade mensal de litros abastecidos no período de um ano (por usuário)
| GET|HEAD      |   api/reports/amount-by-month              |    reports.amount-by-month               | Quantidade mensal paga no período de um ano (por usuário)
| GET|HEAD      |   api/reports/kilometers-by-month-vehicle  |    reports.kilometers-by-month-vehicle   | Quilômetros rodados mensalmente no período de um ano (por usuário e agrupado por veículo)
| GET|HEAD      |   api/reports/kilometers-by-month          |    reports.kilometers-by-month           | Quilômetros rodados mensalmente no período de um ano (por usuário)
| GET|HEAD      |   api/reports/average-spent/{vehicle_id}   |    reports.average-spent                 | Média mensal por carro (por usuário e por veículo - informar)
```

* Login
```
| POST      |   oauth/token   |   passport.token          | Login
```

PS: vale ressaltar, que os métodos INDEX e o SHOW, todos eles (ou quase todos), fiz de uma maneira onde pelo proprio parametro é possível trazer os relacionamentos, evitando a criação de métodos desnecessários.
ex: GET -> api/vehicle/1?with=model,model.brand,typeVehicle,user
Retorno: 
```
{
    "status": "success",
    "data": {
        "id": 1,
        "user_id": 1,
        "file_id": null,
        "model_id": 42,
        "type_vehicle_id": 1,
        "year": 2014,
        "mileage": 183623,
        "license_plate": "WHR9482",
        "type_fuel": "D",
        "active": 1,
        "created_at": "2020-07-16T20:55:24.000000Z",
        "updated_at": "2020-07-16T20:55:24.000000Z",
        "model": {
            "id": 42,
            "brand_id": 157,
            "name": "BUGGY",
            "active": 1,
            "created_at": null,
            "updated_at": null,
            "brand": {
                "id": 157,
                "name": "BUGGY",
                "active": 1,
                "created_at": null,
                "updated_at": null
            }
        },
        "type_vehicle": {
            "id": 1,
            "name": "Carro",
            "active": 1,
            "created_at": "2020-07-16T20:55:23.000000Z",
            "updated_at": "2020-07-16T20:55:23.000000Z"
        },
        "user": {
            "id": 1,
            "name": "Ana Flávia Araújo Carvalho",
            "email": "cokitabr@gmail.com",
            "active": 1,
            "created_at": "2020-07-16T20:55:21.000000Z",
            "updated_at": "2020-07-17T01:19:37.000000Z"
        }
    }
}
```
