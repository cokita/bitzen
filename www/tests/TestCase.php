<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;

    /**
     * If true, setup has run at least once.
     *
     * @var boolean
     */
    protected static $setUpRun = false;

    public function setUp():void {
        parent::setUp();

        if (!static::$setUpRun) {
            $this->artisan('migrate:refresh');
            $this->artisan('db:seed');
            Artisan::call( 'passport:install' );

            static::$setUpRun = true;
        }

        $this->beginDatabaseTransaction();
    }

}
