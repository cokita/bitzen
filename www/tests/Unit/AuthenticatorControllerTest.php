<?php

namespace Authenticator\Tests\Controllers;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AuthenticatorControllerTest extends TestCase {

    public function testOauthToken()
    {
        $oauth = DB::select(DB::raw('select * from oauth_clients where id = 1'));
        if(!$oauth)
            return false;

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ])->json('POST', '/oauth/token', [
            "grant_type" => "client_credentials",
            "client_id" => 1,
            "client_secret" => $oauth[0]->secret
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'token_type', 'expires_in', 'access_token'
        ]);

    }

    public function testNoAuthorization() {
        $response = $this->get('api/user/1');
        $response->assertExactJson(['message' => 'Unauthenticated.']);
    }

    public function testAuthorization() {
        $oauth = DB::select(DB::raw('select * from oauth_clients where id = 2'));
        $response = $this->withHeaders([
            'Content-Type' => 'application/json'
        ])->json('POST', '/oauth/token', [
            "grant_type" => "password",
            "client_id" => 2,
            "client_secret" => $oauth[0]->secret,
            "username" => "cokitabr@gmail.com",
	        "password" => "abcd1234"
        ]);

        $token = $response->json();
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. $token['access_token']
        ])->json('GET', '/api/user/1');


        $response->assertStatus(200);
    }

}
