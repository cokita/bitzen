<?php

namespace Authenticator\Tests\Controllers;

use Faker\Factory;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UserControllerTest extends TestCase {

    public function testStoreUser()
    {
        $faker = Factory::create();
        $user = factory(\App\Models\User::class)->create();
        Passport::actingAs($user);

        $response = $this->postJson('/api/user', [
            'email' => $faker->unique()->email,
            'name' => $faker->name,
            'password' => $faker->password]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'status', 'data'
            ]);


    }

}
