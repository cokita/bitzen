<?php

namespace Authenticator\Tests\Controllers;

use App\Models\Vehicle;
use Faker\Factory;
use Laravel\Passport\Passport;
use Tests\TestCase;

class VehiclesControllerTest extends TestCase {

    public function testStoreVehicle()
    {
        $user = factory(\App\Models\User::class)->create();
        Passport::actingAs($user);

        $vehicle = factory(Vehicle::class)->make();

        $response = $this->postJson('/api/vehicle', $vehicle->toArray());

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'status', 'data'
            ]);
    }

    public function testStoreVehicleValidation()
    {
        $user = factory(\App\Models\User::class)->create();
        Passport::actingAs($user);

        $vehicle = factory(Vehicle::class)->make();

        $response = $this->postJson('/api/vehicle', []);

        $response
            ->assertStatus(400)
            ->assertExactJson(['status'=> 'error', 'message' => 'O modelo do veículo é obrigatório']);
    }

}
