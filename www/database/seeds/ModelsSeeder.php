<?php

use Illuminate\Database\Seeder;

class ModelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $totalModels = \App\Models\Models::query()->count();
        if($totalModels <= 0) {
            $path = 'database/scripts/models.sql';
            DB::unprepared(file_get_contents($path));
            $this->command->info('Models table seeded!');
        }
    }
}
