<?php

use Illuminate\Database\Seeder;

class BrandTypeVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brandsTypeVehicle = \App\Models\BrandTypeVehicle::query()->count();
        if($brandsTypeVehicle <= 0) {
            $path = 'database/scripts/brand_type_vehicle_cars.sql';
            DB::unprepared(file_get_contents($path));
            $this->command->info('BrandTypeVehicle to CARS seeded!');

            $path = 'database/scripts/brand_type_vehicle_motocycles.sql';
            DB::unprepared(file_get_contents($path));
            $this->command->info('BrandTypeVehicle to MOTOCYCLES seeded!');

            $path = 'database/scripts/brand_type_vehicle_trucks.sql';
            DB::unprepared(file_get_contents($path));
            $this->command->info('BrandTypeVehicle to TRUCKS seeded!');

            $path = 'database/scripts/brand_type_vehicle_nauticals.sql';
            DB::unprepared(file_get_contents($path));
            $this->command->info('BrandTypeVehicle to NAUTICALS seeded!');
        }
    }
}
