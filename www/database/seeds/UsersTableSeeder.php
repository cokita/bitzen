<?php

use Illuminate\Database\Seeder;
use \App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['email' => 'cokitabr@gmail.com',
            'name' => 'Ana Flávia Carvalho',
            'password' => bcrypt('abcd1234')]);

        User::create(['email' => 'andrevini@gmail.com',
            'name' => 'André Vincius da Silva Caixeta',
            'password' => bcrypt('abcd1234')]);

        User::create(['email' => 'gigiovana.caixeta@gmail.com',
            'name' => 'Giovana Araújo Carvalho da Silva Caixeta',
            'password' => bcrypt('abcd1234')]);

        User::create(['email' => 'mateuscarvalhocaixeta@gmail.com',
            'name' => 'Mateus Araújo Carvalho da Silva Caixeta',
            'password' => bcrypt('abcd1234')]);

        User::create(['email' => 'catarinacarvalhocaixeta@gmail.com',
            'name' => 'Catarina Araújo Carvalho da Silva Caixeta',
            'password' => bcrypt('abcd1234')]);

        /*User::create(['email' => 'anaflavia.alpc@gmail.com',
            'name' => 'Ana Flávia Carvalho',
            'password' => bcrypt('abcd1234')]);

        User::create(['email' => 'cokitabr2@gmail.com',
            'name' => 'Ana Carvalho 2',
            'password' => bcrypt('abcd1234')]);

        User::create(['email' => 'cokitabr3@gmail.com',
            'name' => 'Ana Carvalho 3',
            'password' => bcrypt('abcd1234')]);

        User::create(['email' => 'cokitabr4@gmail.com',
            'name' => 'Ana Carvalho 4',
            'password' => bcrypt('abcd1234')]);*/
    }
}
