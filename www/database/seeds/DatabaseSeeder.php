<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(ModelsSeeder::class);
        $this->call(TypeVehicleSeeder::class);
        $this->call(BrandTypeVehicleSeeder::class);
         $this->call(VehiclesSeeder::class);
         $this->call(SuppliesSeeder::class);
    }
}
