<?php

use Illuminate\Database\Seeder;
use \App\Models\TypeVehicle;

class TypeVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeVehicle::create(['id' => 1, 'name' => 'Carro']);
        TypeVehicle::create(['id' => 2, 'name' => 'Moto']);
        TypeVehicle::create(['id' => 3, 'name' => 'Caminhão']);
        TypeVehicle::create(['id' => 4, 'name' => 'Náutica']);
    }
}
