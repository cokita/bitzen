<?php

use Illuminate\Database\Seeder;
use \App\Models\Vehicle;
use \App\Models\BrandTypeVehicle;
use \App\Models\Models;
use \App\Models\User;
use \App\Constants\ConstTypeVehicle;

class VehiclesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *{
     *      "model_id": "andrevini@gmail.com",
     *      "type_vehicle_id" : 1,
     *      "year": 2020,
     *      "mileage": 65000,
     *      "license_plate": "JPQ1112",
     *      "type_fuel": "F"
     *   }
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $users = User::query()->get();

        foreach ($users as $user) {
            $this->createVehicle(ConstTypeVehicle::CAR, $user);
        }

        foreach ($users as $user) {
            $this->createVehicle(ConstTypeVehicle::MOTOCYCLE, $user);
        }

        foreach ($users as $user) {
            $this->createVehicle(ConstTypeVehicle::TRUCK, $user);
        }
    }

    private function createVehicle($type, $user) {
        $randomPlate = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 3);
        $randomFuel= substr(str_shuffle("FGAD"), 0, 1);
        $model = self::getModel($type);

        Vehicle::create([
            'user_id' => $user->id,
            'model_id' => $model->id,
            'type_vehicle_id' => 1,
            'year' => rand( 2000 , date("Y")),
            'mileage' => rand( 0 , 200000),
            'license_plate' => strtoupper($randomPlate).rand( 1000 , 9999),
            'type_fuel' => $randomFuel
        ]);
    }

    public static function getModel($type){
        $brand = BrandTypeVehicle::query()->where("type_vehicle_id", $type)->inRandomOrder()->first();
        $model = Models::query()->where('brand_id', $brand->brand_id)->inRandomOrder()->first();
        if(!$model) {
            return self::getModel($type);
        }

        return $model;
    }
}
