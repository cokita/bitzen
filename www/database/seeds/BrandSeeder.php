<?php

use Illuminate\Database\Seeder;
use \App\Models\Brands;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = Brands::query()->count();
        if($brands <= 0) {
            $path = 'database/scripts/brands.sql';
            DB::unprepared(file_get_contents($path));
            $this->command->info('Brands table seeded!');
        }
    }
}
