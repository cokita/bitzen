<?php

use Illuminate\Database\Seeder;
use  \App\Models\Supplies;
use \App\Models\User;
use \App\Models\Vehicle;

class SuppliesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Tipos de Combustivel: G -> Gasolina, A -> Alcool, D -> Diesel, E -> Eletrico, GN -> Gas Natural
     * @return void
     */
    public function run()
    {
        $vehicles = Vehicle::query()->get();
        foreach ($vehicles as $vehicle) {
            for($x = 0; $x <= date('j');  $x++ ){
                for($i = 0; $i < 4; $i++) {
                    $randomFuel= substr(str_shuffle("GAD"), 0, 1);
                    $lastSupplie = Supplies::query()->where('vehicle_id', $vehicle->id)->orderByDesc('created_at')->first();
                    if($lastSupplie){
                        $current_mileage = ($lastSupplie->liters*$this->rand_float(9,15, 20))+$lastSupplie->current_mileage;
                        $date = strtotime("+7 days", $lastSupplie->created_at->timestamp);
                    }else{
                        $current_mileage = $vehicle->mileage;
                        $date = strtotime("1 January 2020");
                    }

                    $amount = rand(20,300);
                    Supplies::create([
                        'user_id' => $vehicle->user_id,
                        'vehicle_id' => $vehicle->id,
                        'type_fuel' => $randomFuel,
                        'amount' => $amount,
                        'current_mileage' => $current_mileage,
                        'liters' => ($amount/3.95),
                        'fuel_station' => "Posto de Combustível ".rand( 0 , 9999),
                        'created_at' => $date
                    ]);
                }
            }
        }
    }

    public function rand_float($st_num=0,$end_num=1,$mul=1000000)
    {
        if ($st_num>$end_num) return false;
        return mt_rand($st_num*$mul,$end_num*$mul)/$mul;
    }
}
