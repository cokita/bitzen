<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('file_id')->nullable();
            $table->unsignedInteger('model_id');
            $table->unsignedInteger('type_vehicle_id');
            $table->integer('year');
            $table->bigInteger('mileage');
            $table->string('license_plate', 15);
            $table->char('type_fuel', 2); //F -> Flex, G -> Gasolina, A -> Alcool, D -> Diesel, E -> Eletrico
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('model_id')->references('id')->on('models');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('file_id')->references('id')->on('files');
            $table->foreign('type_vehicle_id')->references('id')->on('type_vehicle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
