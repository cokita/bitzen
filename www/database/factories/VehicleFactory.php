<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Vehicle::class, function (Faker $faker) {
    $user = \App\Models\User::query()->inRandomOrder()->first();
    $randomPlate = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 3);
    $randomFuel= substr(str_shuffle("FGAD"), 0, 1);
    $model = VehiclesSeeder::getModel(\App\Constants\ConstTypeVehicle::CAR);
    return [
        'user_id' => $user->id,
        'model_id' => $model->id,
        'type_vehicle_id' => 1,
        'year' => rand( 2000 , date("Y")),
        'mileage' => rand( 0 , 200000),
        'license_plate' => strtoupper($randomPlate).rand( 1000 , 9999),
        'type_fuel' => $randomFuel
    ];
});



