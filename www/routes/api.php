<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => ['json.response', 'auth:api']], function () {
    Route::group(['prefix' => 'brands', 'as' => 'brands.'], function () {
        Route::get('/brands-by-type-vehicle/{id}', ['as' => 'brands-by-type-vehicle', 'uses' => 'BrandsController@brandsByTypeVehicle']);
    });
    Route::group(['prefix' => 'file', 'as' => 'file.'], function () {
        Route::post('/{id}', ['as' => 'show', 'uses' => 'FileController@show']);
        Route::post('/', ['as' => 'store', 'uses' => 'FileController@store']);
    });
    Route::group(['prefix' => 'vehicle', 'as' => 'vehicle.'], function () {
        Route::get('/only-mine', ['as' => 'only-mine', 'uses' => 'VehicleController@listOnlyMyVehicles']);
    });

    Route::group(['prefix' => 'supplies', 'as' => 'supplies.'], function () {
        Route::get('/only-mine', ['as' => 'only-mine', 'uses' => 'SuppliesController@listOnlyMySupplies']);
    });

    Route::group(['prefix' => 'reports', 'as' => 'reports.'], function () {
        Route::get('/liters-by-month', ['as' => 'liters-by-month', 'uses' => 'ReportsController@reportLitersByMonth']);
        Route::get('/amount-by-month', ['as' => 'amount-by-month', 'uses' => 'ReportsController@reportAmountByMonth']);
        Route::get('/kilometers-by-month-vehicle', ['as' => 'kilometers-by-month-vehicle', 'uses' => 'ReportsController@reportKilometersByMonthByCar']);
        Route::get('/kilometers-by-month', ['as' => 'kilometers-by-month', 'uses' => 'ReportsController@reportKilometersByMonthAllCars']);
        Route::get('/average-spent/{vehicle_id}', ['as' => 'average-spent', 'uses' => 'ReportsController@reportAverageSpent']);
    });

    Route::resource('user', 'UserController');
    Route::resource('type-vehicles', 'TypeVehicleController', ['only' => ['show', 'index']]);
    Route::resource('brands', 'BrandsController', ['only' => ['show', 'index']]);
    Route::resource('models', 'ModelsController', ['only' => ['show', 'index']]);
    Route::resource('vehicle', 'VehicleController');
    Route::resource('supplies', 'SuppliesController');
    //Route::resource('file', 'FileController', ['only' => ['show', 'index']]);
});

